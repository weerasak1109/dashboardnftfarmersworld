﻿using Dashboard.Helpers;
using Dashboard.Models;
using Dashboard.Models.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static Dashboard.Helpers.MessengerError;

namespace Dashboard.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            IndexViewModel indexViewModel = new IndexViewModel();
            try
            {
            } 
            catch (Exception ex)
            {
                throw ex;
            }
            return View(indexViewModel);
        }

        // POST: Index 
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Index")]
        public async Task<ActionResult> Index(IndexViewModel indexView)
        {
            try
            {
                if (string.IsNullOrEmpty(indexView.Txt_IdWaxSearch))
                {
                    ViewBag.MesWarning = string.Format("{0}", Error.idWaxNull);
                    return View(indexView);
                }

                string iUrlAssets = @"https://wax.api.atomicassets.io/atomicassets/v1/assets?page=1&limit=1000&collection_name=farmersworld&owner=";
                string iFullUrlAssets = string.Format("{0}{1}", iUrlAssets, indexView.Txt_IdWaxSearch);
                
                TomicAssetsModel iTomicAssets = GetTomicAssets(@iFullUrlAssets);
                if (iTomicAssets != null && iTomicAssets.success == true)
                {
                    indexView.CountFarmerCoin = iTomicAssets.data.Count();
                }

                string iUrlBalances = @"https://lightapi.eosamsterdam.net/api/balances/wax/";
                string iFullUrlBalances = string.Format("{0}{1}", iUrlBalances, indexView.Txt_IdWaxSearch);
                BalancesModel iTomicBalances = GetTomicBalances(iFullUrlBalances);
                if (iTomicBalances != null)
                {
                    indexView.NameWalletAddress = iTomicBalances.account_name;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(indexView);
        }




        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }






        /// <summary>
        /// ดึงของที่มีอยู่ในกระเป๋า NFT
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public TomicAssetsModel GetTomicAssets(string url)
        {
            TomicAssetsModel data = null;
            try
            {
                if (!string.IsNullOrEmpty(url))
                {
                    // Create a request for the URL.
                    WebRequest request = WebRequest.Create(url);
                    // If required by the server, set the credentials.
                    request.Credentials = CredentialCache.DefaultCredentials;

                    // Get the response.
                    WebResponse response = request.GetResponse();
                    // Display the status.
                    Console.WriteLine(((HttpWebResponse)response).StatusDescription);

                    // Get the stream containing content returned by the server.
                    // The using block ensures the stream is automatically closed.
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        // Open the stream using a StreamReader for easy access.
                        StreamReader reader = new StreamReader(dataStream);
                        // Read the content.
                        string responseFromServer = reader.ReadToEnd();

                        if (!string.IsNullOrEmpty(responseFromServer))
                        {
                            data = new TomicAssetsModel();
                            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                            data = jsonSerializer.Deserialize<TomicAssetsModel>(responseFromServer);
                        }
                        
                        // Display the content.
                        reader.Close();
                    }

                    // Close the response.
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public BalancesModel GetTomicBalances(string url)
        {
            BalancesModel data = null;
            try
            {
                if (!string.IsNullOrEmpty(url))
                {
                    // Create a request for the URL.
                    WebRequest request = WebRequest.Create(url);
                    // If required by the server, set the credentials.
                    request.Credentials = CredentialCache.DefaultCredentials;

                    // Get the response.
                    WebResponse response = request.GetResponse();
                    // Display the status.
                    Console.WriteLine(((HttpWebResponse)response).StatusDescription);

                    // Get the stream containing content returned by the server.
                    // The using block ensures the stream is automatically closed.
                    using (Stream dataStream = response.GetResponseStream())
                    {
                        // Open the stream using a StreamReader for easy access.
                        StreamReader reader = new StreamReader(dataStream);
                        // Read the content.
                        string responseFromServer = reader.ReadToEnd();

                        if (!string.IsNullOrEmpty(responseFromServer))
                        {
                            data = new BalancesModel();
                            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                            data = jsonSerializer.Deserialize<BalancesModel>(responseFromServer);
                        }

                        // Display the content.
                        reader.Close();
                    }

                    // Close the response.
                    response.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }



        //https://lightapi.eosamsterdam.net/api/balances/wax/2p4gu.wam
    }
}