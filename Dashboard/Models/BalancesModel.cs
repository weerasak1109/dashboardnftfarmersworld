﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard.Models
{
    public class BalancesModel
    {
        public List<Balance> balances { get; set; }
        public string account_name { get; set; }
        public Chain chain { get; set; }
    }


    public class Balance
    {
        public string currency { get; set; }
        public string decimals { get; set; }
        public string contract { get; set; }
        public string amount { get; set; }
    }

    public class Chain
    {
        public int rex_enabled { get; set; }
        public string description { get; set; }
        public string block_time { get; set; }
        public string chainid { get; set; }
        public int sync { get; set; }
        public int decimals { get; set; }
        public int block_num { get; set; }
        public int production { get; set; }
        public string network { get; set; }
        public string systoken { get; set; }
    }
}