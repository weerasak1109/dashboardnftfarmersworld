﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard.Models.Views
{
    public class IndexViewModel
    {
        public string Txt_IdWaxSearch { get; set; }
        public int CountFarmerCoin { get; set; } = 0;
        public string NameWalletAddress { get; set; }
        public double CountFWWCoin { get; set; } = 0;
        public double CountFWGCoin { get; set; } = 0;
        public double CountFWFCoin { get; set; } = 0;
    }
}