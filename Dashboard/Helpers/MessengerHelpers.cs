﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dashboard.Helpers
{
    public class MessengerError
    {
        public class Success
        {
            public static string saveSuccess => "บันทึกสำเร็จ";
        }

        public class Error
        {
            public static string systemError => "เกิดข้อผิดพลาดจากระบบ กรุณาติดต่อผู้ดูแลระบบ";
            public static string idWaxNull => "กรุณาระบุ Wallet Address (ตัวอย่าง zxc.wam) ";
        }

    }
}